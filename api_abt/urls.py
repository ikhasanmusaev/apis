from django.urls import path

from api_abt import views

app_name = 'api_abt'

urlpatterns = [
    path('quota', views.quota),
]
