from django.http import HttpResponse
from rest_framework.decorators import api_view

import json
import datetime
import requests
from rest_framework.response import Response

url = 'http://qabul.dtm.uz/data/day.json?v='


def return_url(s_url):
    time = datetime.datetime.now().strftime('%Y%m%d%H')[2:]
    return s_url + time


def return_data(all_data):
    final_list = []
    for j_key, j in all_data.items():  # j = university

        for k_key, k in j.items():  # k = language

            for l_key, l in k.items():  # l = emode

                for m_key, m in l.items():
                    my_dict = m.copy()
                    my_dict['university'] = j_key
                    my_dict['language'] = k_key
                    my_dict['emode'] = l_key

                    final_list.append(my_dict)
                    my_dict = {}
    return final_list


@api_view(['GET'])
def quota(request):
    r = requests.get(return_url(url), verify=False)
    print(return_url(url))
    my_data = json.loads(r.text)
    final_data = return_data(my_data)
    return Response(final_data)
